package ap.creational.factorypattern;

public abstract class Car {

	CarType carType;

	abstract void assembel();

	abstract void paint();

	public Car(CarType carType) {
		this.carType = carType;
		this.arrangeParts();
	}

	private void arrangeParts() {
		System.out.println("Arranging parts for " + getCarType() + " car");
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

}
