package ap.creational.factorypattern;

public class SmallCar extends Car {

	private CarColour colour;

	public SmallCar(CarColour colour) {
		super(CarType.SMALL);
		this.colour = colour;
		assembel();
		paint();
	}

	@Override
	void assembel() {
		System.out.println("Assembel Small Car ");

	}

	@Override
	void paint() {
		System.out.println("Painted colour is " + colour);

	}

}
