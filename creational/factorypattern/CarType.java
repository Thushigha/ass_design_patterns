package ap.creational.factorypattern;

public enum CarType {

	SMALL, SEDAN, LUXURY

}
