package ap.behavioural.observerpattern;

public interface WeatherObserver {
	public void doUpdate(int temperature);
}
