package ap.creational.factorypattern;

public class SedanCar extends Car {

	private CarColour colour;

	public SedanCar(CarColour colour) {
		super(CarType.SEDAN);
		this.colour = colour;
		assembel();
		paint();
	}

	@Override
	void assembel() {
		System.out.println("Assembel Sedan Car ");

	}

	@Override
	void paint() {
		System.out.println("Painted colour is " + colour);

	}

}
