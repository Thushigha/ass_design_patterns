package ap.creational.factorypattern;

public class CarFactoryDemo {
	public static void main(String[] args) {

		CarFactory.buildCar(CarType.LUXURY, CarColour.BLACH);
		System.out.println("---------------------------------");
		CarFactory.buildCar(CarType.SMALL, CarColour.WHITE);
		System.out.println("---------------------------------");
		CarFactory.buildCar(CarType.SEDAN, CarColour.RED);

	}
}
