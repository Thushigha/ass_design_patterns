package ap.creational.factorypattern;

public class CarFactory {

	public static Car buildCar(CarType type, CarColour colour) {

		switch (type) {
		case SMALL:
			return new SmallCar(colour);
		case LUXURY:
			return new LuxuryCar(colour);
		case SEDAN:
			return new SedanCar(colour);
		}

		return null;

	}

}
