package ap.creational.factorypattern;

public class LuxuryCar extends Car {

	private CarColour colour;

	public LuxuryCar(CarColour colour) {
		super(CarType.LUXURY);
		this.colour = colour;
		assembel();
		paint();
	}

	@Override
	void assembel() {
		System.out.println("Assembel Luxury Car ");

	}

	@Override
	void paint() {
		System.out.println("Painted colour is " + colour);

	}

}
